<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Security extends Model
{
    use HasFactory;

    private $users;

    public function __construct()
    {

        //$file = fopen(storage_path("database/user.txt"), "r");

        //$file = "user.txt";
        //$handle = fopen($file, "r");
        $read = file_get_contents(storage_path("database/user.txt"));
        $lines = explode("\n", $read);
        $i = 0;
        $this->users = array();
        //print_r($lines);

        foreach($lines as $value)
        {

                $cols[$i] = explode("\t", $value);
                $u = new User($cols[$i][0],$cols[$i][1]);
                $this->users[$i] = $u;
                $i++;

        }
        //print_r($this->users);

    }

    public function authenticate($email,$password)
    {

        for($i = 0;$i<count($this->users);$i++)
        {
            $user = $this->users[$i];

            if($email == $user->getEmail())
            {
                if(password_verify($password,$user->getPassword()))
                {
                    session()->put("loggedin",1);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }
}
