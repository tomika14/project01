<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    use HasFactory;

    private $randomnumbers;

    public function __construct()
    {
        $this->randomnumbers = array();
    }


    public function generateRandomNumbers()
    {
        $random = mt_rand(1,90);
        if($this->checkIfExistsInArray($random))
        {
            $this->generateRandomNumbers();
        }
        else
        {
            array_push($this->randomnumbers,$random);
        }

    }

    /**
     * @return mixed
     */
    public function getRandomnumbers()
    {
        return $this->randomnumbers;
    }

    /**
     * @param mixed $randomnumbers
     */
    public function setRandomnumbers($randomnumbers): void
    {
        $this->randomnumbers = $randomnumbers;
    }

    private function checkIfExistsInArray($num): bool
    {
        for($i = 0; $i < count($this->randomnumbers); $i++)
        {
            if($num == $this->randomnumbers[$i])
            {
                return true;
            }
        }
        return false;
    }
}
