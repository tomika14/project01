<?php

namespace App\Http\Controllers;

use App\Models\Lottery;
use Illuminate\Http\Request;

class LotteryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lottery  $lottery
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(!session()->exists("loggedin"))
        {
            return redirect("/login");
        }
        //print session()->get("lottery");
        //session()->remove("lottery");
        if(!session()->exists("lottery")){
            session()->put("lottery",0);
        }

        $sess = session()->get("lottery");

        if($sess == 5)
        {
            session()->put("lottery",0);
        }

        if( session("lottery") == 0)
        {

            //session()->put("lottery",$sess);
            $lottery = new Lottery();
            $i = 0;
            while($i != 5)
            {
                $lottery->generateRandomNumbers();
                $i++;
            }
            session()->put("lottery", 1);
            session()->put("numbers", $lottery->getRandomnumbers());
        }
        else
        {
            //print_r(session()->get("numbers"));
            $sess = session()->get("lottery");
            //print $sess;
            session()->put("lottery",($sess+1));
        }
        print session()->get("lottery");

        //$numbers = $lottery->getRandomnumbers();
        //session()->put("numbers",$numbers);



        return view("content", ["numbers" => session()->get("numbers")]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lottery  $lottery
     * @return \Illuminate\Http\Response
     */
    public function edit(Lottery $lottery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lottery  $lottery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lottery $lottery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lottery  $lottery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lottery $lottery)
    {
        //
    }
}
