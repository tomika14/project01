<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login</title>
</head>
<body>
@isset($success)
    @if($success)
        Sikeres bejelentkezés
    @else
        <strong>Hibás felhasználónév/jelszó</strong>
    @endif
@endisset
<br />

admin@admin.hu<br />
admin123

    <form method="post" action="/authenticate">
    @csrf {{ csrf_field() }}

        <input type="email" name="email" placeholder="E-mail cím" required>
        <input type="password" name="password" placeholder="Jelszó" required>
        <button type="submit">Bejelentkezés</button>
    </form>
</body>
</html>
