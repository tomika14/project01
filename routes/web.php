<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (session()->exists("loggedin"))
    {
        return redirect("/lottery");
    }else
    {
        return redirect("/login");
    }
});
Route::get("/lottery",[\App\Http\Controllers\LotteryController::class,"show"]);
Route::get("/login",function (){
   return view("login");
});
Route::post("/authenticate",[\App\Http\Controllers\SecurityController::class,"login"]);
Route::get("/logout",[\App\Http\Controllers\SecurityController::class,"logout"]);

